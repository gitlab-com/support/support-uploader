# Create / Upload GitLabSOS

See [GitLabSOS](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos) for more details

Usage

```
/opt/gitlab/embedded/bin/curl https://gitlab.com/gitlab-com/support/toolbox/gitlabsos/raw/master/gitlabsos.rb -o gitlabsos.rb
/opt/gitlab/embedded/bin/ruby gitlabsos.rb --support-uploader '%%BASE%%'
```
