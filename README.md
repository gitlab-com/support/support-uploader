# Support-Uploader

The Support Uploader project solves the 20MB file upload limit to Zendesk tickets, allowing customers to send us large files while allowing Gitlab to retain full control of customer upload data.

## How does it work?

When a customer wants to send us a large file the following occurs:
 
1. The Gitlab Support Engineer generates upload tools for the customer from [uploader.gitlab.support](https://uploader.gitlab.support/)
1. The Gitlab Support Engineer creates a public response using the `General::Support uploader` macro in Zendesk and attaches the upload tools zip file generated.
1. The Customer can use either the html file or the shell script to upload the file to Support. 
1. When the file is uploaded the ticket is updated with an internal comment containing a link to the uploaded file.
1. The link brings the Gitlab Support Engineer to the file in the S3 bucket in the Support Uploader AWS account.
1. The Gitlab Support Engineer downloads the customer file from the S3 console and works with it as normal.

![MVP architecture](architecture/mvp_support_uploader.png)

## Details
More detailed information for Gitlab Support Engineers and Customers is documented in these Handbook pages:
* [Instructions for Gitlab Support Engineers](https://about.gitlab.com/handbook/support/workflows/large_files.html)
* [Instructions for Customers](https://about.gitlab.com/support/providing-large-files.html)


